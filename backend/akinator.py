import openai

from tenacity import (
    retry,
    stop_after_attempt,
    wait_random_exponential,
)  # for exponential backoff

@retry(wait=wait_random_exponential(min=1, max=60), stop=stop_after_attempt(6))
def completion_with_backoff(**kwargs):
    return openai.ChatCompletion.create(**kwargs)

openai.api_key = "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"

with open('база.txt', 'r', encoding='utf-8') as f:
    system = f.read()

messages = [{'role':'system',
             'content':system}]
guesses = []
while True:

    try:  
        completion = completion_with_backoff(
            model="gpt-3.5-turbo",
            messages=messages,
            temperature=0.4
            )
        ask = completion.choices[0]['message']['content']
        messages.append({'role':'assistant',
                         'content':ask})
        print(ask, completion.usage['total_tokens'])
        if 'угадал!' in ask.lower():
            guesses.append(messages[-3])
            messages = [{'role':'system',
                         'content':system}]
            print()
            cont = input('Хотите начать заново? ')
            if cont.lower() == 'да':
                continue
            else:
                print('Пока!')
                break
        answer = input()
        print()
        messages.append({'role':'assistant',
                         'content':answer})
        
    except Exception as e:
        print(e)
    
    
