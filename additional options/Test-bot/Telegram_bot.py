# Добавляем библиотеки для кода
import os
import telebot
import keep_alive
import guesser
# Вместо строки в кавычках нужно вставить токен, присваиваемый боту (тоже в кавычках)
TOKEN = os.environ['BOT_TOKEN']

bot = telebot.TeleBot(TOKEN)

print("Starting the bot")
# Список для записи id игроков (первичных пользований)
players = []
# Функция, порождающая привественное сообщение для команд help и start
@bot.message_handler(commands=['help', 'start'])
def send_welcome(message):
    bot.send_message(message.from_user.id, "Привет! Я HSEkinator, и я умею угадывать обитателей Вышки, прямо как Акинатор!")
# Функция для игры и взаимодействия с ответами пользователя
@bot.message_handler(commands=['play'])
def start_game(message):
    id = message.from_user.id
    if (id in list(guesser.msg)):
        del guesser.msg[id]
    if (id not in players):
        players.append(id)
    bot.send_message(id, "Начинаем новую игру!")
# Создание клавиш для ответов на вопросы
    keyboard = telebot.util.quick_markup({
        'Да': {'callback_data': 'Да'},
        'Скорее да': {'callback_data': 'Скорее да'},
        'Не знаю': {'callback_data': 'Не знаю'},
        'Скорее нет': {'callback_data': 'Скорее нет'},
        'Нет': {'callback_data': 'Нет'},
    }, row_width=5)
    bot.send_chat_action(id, action="typing")
    bot.send_message(id, guesser.response(id)[1], reply_markup=keyboard)
# Функция, описывающая реакции бота:
@bot.message_handler(func=lambda message: True)
def echo_message(message):
    id = message.from_user.id
    if (id not in players):
        bot.send_message(id, "Введите /play, чтобы начать!")
    else:
        bot.send_chat_action(id, action="typing")
        resp = guesser.response(id, message.text)
        if (resp[0]):
            bot.send_message(id, "Ура, я угадал! Напишите /play, чтобы играть снова.")
            players.remove(id)
        else:
            bot.send_message(id, resp[1])
        
# Предотвращаем разрыв соединения
keep_alive.keep_alive()
bot.infinity_polling()
