# Импортируем библиотеки с функциями для интерфейса и для искусственного интеллекта
import eel
import openai
# Задаём доступ к нейросетке
openai.api_key = "sk-mPAvfoAY1qEwmPspDtfzT3B1bkFLNZ4TcX20DMuOjRTfkH"
# Папка с рабочими файлами
eel.init('web')
# Как я поняла, этот декоратор нужен, чтобы нас поняла Java                    
@eel.expose                         
def handleinput(x):
    line = '% s' % x
    messages=[]
    messages.append({'role': 'system',
                     'content': line})
    try:
        completion = openai.ChatCompletion.create(
            model="gpt-3.5-turbo",
            messages=messages,
            temperature=0.4)
        ask = completion.choices[0]['message']['content']
        messages.append({'role': 'assistant',
                         'content': ask})
        print(ask + str(completion.usage['total_tokens']))
        messages.append({'role': 'assistant',
                         'content': ask})
        eel.answer(line)

    except:
        #eel.answer('Извините, похоже закончились токены, Акинатор перезапустится')
        eel.answer(line)


# Запускаем
eel.start('main.html', size=(500, 1000))   
