# отличие версий 2 от предыдущих заключается в том, что теперь токены 
# хранятся не в открытом доступе в коде, а помещаются при запуске кода 
# на устройстве в переменные среды

from os import getenv  # импортируем из пакета os модуль getenv, чтобы защитить токены

from telebot import TeleBot  # импортируем из пакета telebot модули для работы с ботом
from telebot.types import Message
import openai  # импортируем библиотеку openai для работы с нейросетью


OPENAI_TOKEN = getenv('OPENAI_TOKEN')  # присваиваем переменным значения переменных среды
TELEGRAM_TOKEN = getenv('TELEGRAM_TOKEN')

openai.api_key = OPENAI_TOKEN

bot = TeleBot(token=TELEGRAM_TOKEN)  # создаем бота
chat_histories = {}  # заводим словарь, чтобы хранить сообщения

# команды, написанные в бэкенде, преобразовываем в функции

def get_gpt_answer(chat_id: int):  
    return openai.ChatCompletion.create(model='gpt-3.5-turbo',
                                        messages=chat_histories[chat_id],
                                        temperature=0.4)['choices'][0]['message']['content']

# message_handler — это декоратор, который реагирует на входящие сообщения и содержит в себе функцию ответа

@bot.message_handler(commands=['start'])  # используем декоратор, чтобы создать команду start
def start(message: Message):
    global chat_histories

    with open('data.txt', mode='r', encoding='utf-8') as f:
        instructions = f.read()

    chat_histories[message.chat.id] = [{'role': 'system',
                                        'content': instructions}]

    response = get_gpt_answer(message.chat.id)
    
    bot.send_message(message.chat.id, response)
    chat_histories[message.chat.id].append({'role': 'assistant',
                                            'content': response})
    

@bot.message_handler()  # используем декоратор 
def game(message: Message):
    global chat_histories

    chat_histories[message.chat.id].append({'role': 'user',
                                            'content': message.text})

    response = get_gpt_answer(message.chat.id)
    
    bot.send_message(message.chat.id, response)
    chat_histories[message.chat.id].append({'role': 'assistant',
                                            'content': response})


bot.infinity_polling()  # запускаем бесконечный цикл, чтобы можно было играть сколько угодно
