from os import getenv

from telebot import TeleBot
from telebot.types import Message
import openai


OPENAI_TOKEN = getenv('OPENAI_TOKEN')
TELEGRAM_TOKEN = getenv('TELEGRAM_TOKEN')

openai.api_key = OPENAI_TOKEN

bot = TeleBot(token=TELEGRAM_TOKEN)
chat_histories = {}


def get_gpt_answer(chat_id: int):
    return openai.ChatCompletion.create(model='gpt-3.5-turbo',
                                        messages=chat_histories[chat_id],
                                        temperature=0.4)['choices'][0]['message']['content']


@bot.message_handler(commands=['start'])
def start(message: Message):
    global chat_histories

    with open('data.txt', mode='r', encoding='utf-8') as f:
        instructions = f.read()

    chat_histories[message.chat.id] = [{'role': 'system',
                                        'content': instructions}]

    response = get_gpt_answer(message.chat.id)
    
    bot.send_message(message.chat.id, response)
    chat_histories[message.chat.id].append({'role': 'assistant',
                                            'content': response})
    

@bot.message_handler()
def game(message: Message):
    global chat_histories

    chat_histories[message.chat.id].append({'role': 'user',
                                            'content': message.text})

    response = get_gpt_answer(message.chat.id)
    
    bot.send_message(message.chat.id, response)
    chat_histories[message.chat.id].append({'role': 'assistant',
                                            'content': response})


bot.infinity_polling()
