# Библиотека для переменных среды
from os import getenv

# Библиотеки для телеграм бота
from telebot import TeleBot
from telebot.types import Message

# Библиотека для повторения запросов к GPT
from tenacity import (
    retry,
    stop_after_attempt,
    wait_random_exponential,
)

# Библиотека для ChatGPT
import openai

OPENAI_TOKEN = getenv('OPENAI_TOKEN')
TELEGRAM_TOKEN = getenv('TELEGRAM_TOKEN')

# Здесь размещаем нашу кодовую молитву, чтобы Бог защитил наш код от падений, багов и прочая скверны
# Это принципиально важная строка. Пожалуйста, не удаляйте её, даже если вы не верующий

openai.api_key = OPENAI_TOKEN

bot = TeleBot(token=TELEGRAM_TOKEN)
chat_histories = {}

# Повторение запросов к ChatGPT, если возникнет ошибка из-за превышения лимита сообщений
@retry(wait=wait_random_exponential(min=1, max=60))
def completion_with_backoff(**kwargs):
    return openai.ChatCompletion.create(**kwargs)

# Начало диалога с ботом по команде /start
@bot.message_handler(commands=['start'])
def start(message: Message):
    global chat_histories
    
    # Чтение инструкций для ChatGPT
    with open('база персонажей.txt', mode='r', encoding='utf-8') as f:
        instructions = f.read()
        
    # Добавление инструкций в историю диалога
    chat_histories[message.chat.id] = [{'role': 'system',
                                        'content': instructions}]
    # Генерация сообщения от ChatGPT
    response = completion_with_backoff(model='gpt-3.5-turbo',
                                       messages=chat_histories[message.chat.id],
                                       temperature=0.4)['choices'][0]['message']['content']
    bot.send_message(message.chat.id, response)
    
    # Добавление вопроса в историю диалога
    chat_histories[message.chat.id].append({'role': 'assistant',
                                            'content': response})

# Получение сообщений от пользователя
@bot.message_handler()
def game(message: Message):
    global chat_histories
    # Добавление сообщения в истрию диалога
    chat_histories[message.chat.id].append({'role': 'user',
                                            'content': message.text})
    
    # Генерация вопроса от ChatGPT
    response = completion_with_backoff(model='gpt-3.5-turbo',
                                       messages=chat_histories[message.chat.id],
                                       temperature=0.4)['choices'][0]['message']['content']
    bot.send_message(message.chat.id, response)
    
    # Добавление вопроса в историю диалога
    chat_histories[message.chat.id].append({'role': 'assistant',
                                            'content': response})
                                            
    # Выдача предсказания на основе загаданного персонажа
    if 'угадал!' in response:
        with open('Предсказания.txt', 'r', encoding='utf-8') as f:
            predprom = {'role': 'system',
                        'content': f.read()}
        predict = completion_with_backoff(model='gpt-3.5-turbo',
                                           messages=[predprom, chat_histories[message.chat.id][-3]],
                                           temperature=0.4)['choices'][0]['message']['content']
        bot.send_message(message.chat.id, predict)


bot.infinity_polling()
